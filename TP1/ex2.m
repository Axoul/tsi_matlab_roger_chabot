close all;
clear variables;

im = imread('journal.png');
%im = imread('images-trames/1-trame.png');
[h,w] = size(im);
imD = im2double(im);

[U,V] = meshgrid(-w/2+1/2:w/2-1/2,-h/2+1/2:h/2-1/2);
D = sqrt(U.^2+V.^2);

Fc = 100;
B = 120;
n = 2;
H = 1./(1+((B.*D)./(D.^2-Fc^2)).^(2*n));
figure(1);
mesh(U,V,H);
title('Affichage 3D du filtre de Butterworth');

TF = fft2(imD);
TFSh = fftshift(TF);

figure(2);
imshow(log10(abs(TFSh)),[]);
title('Transformée de Fourier du Filtre');

TF_Filtree = TFSh.*H;

Im_Filtree = ifft2(ifftshift(TF_Filtree));
figure(3);
subplot(1,2,1);
imshow(imD,[]);
title('Image de Base');
subplot(1,2,2);
imshow(abs(Im_Filtree),[]);
title('Image après Filtrage');