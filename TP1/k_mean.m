close all;
clear variables;

figure(1);
subplot(121)
figure(1);
image_fleur = imread('flower.png');
imD_fleur = im2double(image_fleur);
imshow(image_fleur);

m1=rand(1);
m2=rand(1);
label=zeros(256);
compteur_m1=0;
compteur_m2=0;
somme_m1=0;
somme_m2=0;


i=0;
j=0;
for i=1:256
    for j=1:256
        dist1=abs(imD_fleur(i,j)-m1);
        dist2=abs(imD_fleur(i,j)-m2);
        if dist1<dist2
            label(i,j)=1;
            compteur_m1=compteur_m1+1;
            somme_m1=somme_m1+label(i,j);
        else
            label(i,j)=2;
            compteur_m2=compteur_m2+1;
            somme_m1=somme_m1+label(i,j);
        end
    end
end
subplot(122);
imagesc(label);
colormap('gray');