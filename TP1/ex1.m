close all;
clear variables;

%img_fleur = imread('images/4.png');
img_fleur = imread('flower.png');

figure(1);
imshow(img_fleur,[]);
img_fleurD = im2double(img_fleur);

[h,w] = size(img_fleur);

h1 = [-1 0 1; -2 0 2; -1 0 1];
h2 = h1';

%Gh = imfilter(img_fleurD,h1,'conv');
Gh = imfilter(img_fleurD,h1);
%Gv = imfilter(img_fleurD,h2,'conv');
Gv = imfilter(img_fleurD,h2);
G = sqrt(Gh.^2 + Gv.^2 );

figure(2);
subplot(1,3,1);
imshow(Gh,[]);
title('Gh');
subplot(1,3,2);
imshow(Gv,[]);
title('Gv');
subplot(1,3,3);
imshow(G,[]);
title('G');

imgNoise= imnoise(img_fleurD,'gaussian', 0.01);
Gh2 = imfilter(imgNoise,h1);
Gv2 = imfilter(imgNoise,h2);
G2 = sqrt(Gh2.^2 + Gv2.^2 );

figure(3);
subplot(1,3,1);
imshow(Gh2,[]);
title('Gh');
subplot(1,3,2);
imshow(Gv2,[]);
title('Gv');
subplot(1,3,3);
imshow(G2,[]);
title('G');

Gh_n = Gh2./G2;
Gv_n = Gv2./G2;
d = 2;
[X,Y] = meshgrid(1:w,1:h); 

p1x = round(X + Gh_n*d);
p1x(p1x<1) = 1;
p1x(p1x>256) = 256;

p1y = round(Y + Gv_n*d);
p1y(p1y<1) = 1;
p1y(p1y>256) = 256;

p2x = round(X - Gh_n*d);
p2x(p2x<1) = 1;
p2x(p2x>256) = 256;

p2y = round(Y - Gv_n*d);
p2y(p2y<1) = 1;
p2y(p2y>256) = 256;

C = zeros(h,w);

epsilon = 0.5;

for i = 1:h
    for j = 1:w
        if( ( G2(i,j)- G2(p1x(i,j),p1y(i,j)) > epsilon  ) && ((G2(i,j)-G2(p2x(i,j),p2y(i,j)) > epsilon)))
            C(i,j) = G2(i,j);
        end
    end
end

figure(4);
imshow(C,[]);
title(['Contours de l image bruitee pour d = ',num2str(d)]);

