close all; clear variables;

A=imread('flower.png'); %lecture de l'image
[h,w]=size(A);
figure(1);
imshow(A); %Affichage de l'image
title('Image originale');
m1=floor(256*rand) %Choix aléatoire de l'intensité m1 comprise entre 0 et 255
m2=floor(256*rand) %Choix aléatoire de l'intensité m2 comprise entre 0 et 255
cluster=zeros(size(A)); %Matrice des labels des pixels
m1p=-1; %intensité m1 précédente (initialisée à -1)
m2p=-1; %intensité m2 précédente (initialisée à -1)
while(m1p~=m1)&&(m2p~=m2)  %Jusqu'à ce que m1 et m2 ne changent plus
    for i=1:h %Pour chaque pixel de l'image
        for j=1:w
            if abs(A(j,i)-m1)<abs(A(j,i)-m2) %Si son intensité est plus proche de m1 que de m2
                cluster(j,i)=1; %On lui affecte le label 1
            else
                cluster(j,i)=2; %Sinon on lui affecte le label 2
            end
        end
    end
    m1p=m1; %On stocke m1 dans m1p
    m2p=m2; %On stocke m2 dans m2p
    m1=round(mean(A(cluster==1))) %m1 prend la valeur de la moyenne des intensités des pixels de label 1
    m2=round(mean(A(cluster==2))) %m2 prend la valeur de la moyenne des intensités des pixels de label 2
end

seuil=abs(m1-m2)/2; %On place le seuil au centre du segment [m1;m2]
A((255-A)<seuil)=255; %Si l'intensité d'un pixel est en dessous du seuil, le pixel est blanc
A((255-A)>=seuil)=0; %Si l'intensité du pixel est au dessus du seuil, le pixel est noir
figure(2);
imshow(A,[]);
title('Image seuillée')

