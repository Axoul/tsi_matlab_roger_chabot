\documentclass[12pt,a4paper,french,twoside,openright,onespacing]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{lmodern}	
\renewcommand*\familydefault{\sfdefault}
\usepackage[english,french]{babel}

%----------------------------------------------------------------------------------------
% PACKAGES
%----------------------------------------------------------------------------------------
%\include{packages}
\usepackage{amsmath}
\usepackage{graphicx,floatrow,float}
\usepackage{tcolorbox}
 \usepackage{fancyhdr}
\usepackage[shortlabels]{enumitem}	% personnalisation liste
\usepackage[left=2cm, right=2cm, bottom=2cm, top=2cm]{geometry}	% marges
\usepackage[format=plain,justification=centering]{caption}
\usepackage{tikz}
 
%\geometry{left=2cm, right=2cm, bottom=2cm, top=2cm}
\pagestyle{fancy}
\fancyhead[L]{CPE Lyon -- TSI}
\fancyhead[R]{2019-2020}

\setlist{leftmargin=5.5mm}
%----------------------------------------------------------------------------------------


%----------------------------------------------------------------------------------------
% MACROS
%----------------------------------------------------------------------------------------
\newcommand{\image} [2][]{\includegraphics[#1]{#2}}
%\newcommand{\reponse}[1]{~\\\\%
%	\vspace{0.5cm}
%	\fbox{\begin{minipage}{.98\linewidth}
%			#1
%		\end{minipage}}
%	\vspace{0.5cm}
%}
\newcommand{\reponse}[1]{%
	\vspace{0.1cm}
			#1
	\vspace{0.5cm}
}


\newif\ifsubjectA	% passe-haut en Fourier -> voie lactée
\subjectAfalse 		%\subjectAfalse

\newif\ifsubjectB	% passe-bas en Fourier -> débruitage
\subjectBfalse		%\subjectBfalse

\newif\ifsubjectC	% coupe-bande en Fourier -> trame journal
\subjectCtrue		%\subjectCfalse
%----------------------------------------------------------------------------------------



\begin{document} 

\selectlanguage{french}
\frenchspacing

\begin{center}
	\Large\textbf{Traitement et Synthèse d'Image}\\\Large TP1 -- Filtrage
\end{center}


\section{Filtrage passe-haut dans l'espace direct : détection de contours}

Dans cette partie, on cherche à détecter les contours d'une image à l'aide du filtre de Sobel. 

\subsection{Contours par filtrage de Sobel}

\begin{enumerate}[noitemsep, label=\arabic* --]
	\item Décomposer le filtre de Sobel en produit (matriciel) de deux filtres 1D, dont un filtre de lissage et un filtre dérivatif. Pourquoi a-t-on, dans les filtres de détection de contours, une partie 'lissage' ?
	\reponse{\input{question1-1}}
	
	\item Lire et afficher l'image 'flower.png'. Obtenir les dimensions \texttt{h} et \texttt{w} de l'image.

	\item Calculer les composantes horizontale et verticale du gradient en chaque pixel de l'image, à l'aide du filtre de Sobel et de la fonction de filtrage de Matlab. On enregistrera la composante verticale du gradient de chaque pixel dans la matrice \texttt{Gv} et la composante horizontale dans le tableau \texttt{Gh} (sans utiliser de boucle).
	
	\item En déduire la norme \texttt{G} du gradient en chaque pixel.
Afficher sur une même figure, \texttt{G}, \texttt{Gv} et \texttt{Gh}. %Pour une meilleure visualisation, on pourra utiliser la commande \texttt{imcomplement}. 
Commenter.
	\reponse{\input{question1-4}}
	
	\item Reprendre les questions précédentes en ayant ajouté à l'image de départ un bruit gaussien centré de variance 0.01 en utilisant la fonction matlab \texttt{imnoise}. Qu'observez vous ?
	\reponse{\input{question1-5}}
\end{enumerate}



\subsection{Correction du contours}

\noindent
Les contours des objets correspondent à des maxima locaux dans la direction du gradient. Ainsi, pour chaque pixel de l'image, on calcule les coordonnées des deux points $p_1$ et $p_2$, qui sont à une distance $d$ dans la direction du gradient $\nabla I$ et de l'opposé du gradient $-\nabla I$. Un pixel $p$ appartient alors à un contour si 
\begin{equation}\label{eq:contrainte}
	\text{si } G(p) - G(p_i) > \varepsilon, 
	G(p) > G(p_1) \quad \text{et} \quad G(p) > G(p_2) ,
\end{equation}
où $G$ est la norme du gradient précédemment calculée.

\noindent
Les calculs étant numériques, on remplacera les inégalités \eqref{eq:contrainte} par une condition moins forte de la forme : 
\begin{equation}\label{eq:contrainte-faible}
	\text{si } G(p) - G(p_i) > \varepsilon, 
\end{equation}
avec, par exemple, $\varepsilon=0.5$.

\begin{figure}[H]
	\centering
	\begin{tikzpicture}[scale=2]		
		%-- contour
		\draw[black, thick] plot [smooth, tension=1] coordinates { (0,1) (1,.75) (2,-.5)};
		
		%-- gradient directions and neighbors
		\def\gx{.5}
		\def\gy{.6}
		
		\def\d{2}
		\def\dgx{\d*\gx}
		\def\dgy{\d*\gy}
		
		\draw[] (1,.75) -- +(\dgx,\dgy) node [] {$\bullet$} node [right] {$p_1$};
		\draw[] (1,.75) -- +(-\dgx,-\dgy) node [] {$\bullet$} node [right] {$p_2$};
		
		\draw[->, red!80!black] (1,.75) -- +(\gx,\gy) node [right, midway] {~$\nabla I$};
	 	\draw[->,red!80!black] (1,.75) -- +(-\gx,-\gy) node [right, midway] {$-\nabla I$};
	 	
	 	%-- current point
		\fill[black] (1,.75) node [left] {$p$~~} circle (2pt);
		
		%-- distance
		\draw[<->, shift={(-.2,0.15)}]  (1,.75) -- +(\dgx,\dgy) node [left, midway] {$d$~~};

	\end{tikzpicture}
\end{figure}

\begin{enumerate}[noitemsep, resume,label=\arabic* --]
	\item Calculer les composantes normalisées \texttt{Gv{\_}n} et \texttt{Gh{\_}n} du gradient de l'image bruitée.
	
	\item Pour chaque pixel de l'image, calculer les coordonnées $p_1$ et $p_2$ à partir de \texttt{Gv{\_}n} , \texttt{Gh{\_}n} et $d$. On pourra prendre par exemple $d=2$. Les coordonnées \texttt{X} et \texttt{Y} de $p$ seront obtenues à l'aide de la fonction \texttt{meshgrid}.
	
	\item Enregistrer dans un tableau \texttt{C} la norme du gradient \texttt{G} pour les pixels vérifiant \eqref{eq:contrainte-faible}. On mettra à zéro les autres pixels. 
	
	\item Afficher les contours de l'image bruitée et discuter du résultat en fonction de $d$.
	\reponse{\input{question1-9}}
\end{enumerate}


\section{Filtrage coupe-bande dans l'espace de Fourier}

Dans cette seconde partie, on souhaite supprimer la trame d'un journal. Pour cela, on implémente le filtre de Butterworth, dont on rappelle ci-dessous la définition :
$$H(u,v) =  \dfrac{1}{1 + \left(\dfrac{B \sqrt{u^2 + v^2}}{u^2 + v^2 - f_c^2} \right)^{2n}}$$

\noindent
On pourra par exemple prendre pour paramètres du filtre 
$n=2$, $f_c=100$ et $B=120$ la largeur de bande.

\begin{enumerate}[noitemsep, label=\arabic* --]
	\item Lire et afficher l'image 'journal-trame.png'. Obtenir la taille \texttt{[h,w]} de l'image.
	
	\item Génération du filtre de Butterworth :
	\begin{itemize}[noitemsep]
		\item En utilisant la fonction \texttt{meshgrid}, obtenir deux matrices donnant respectivement les coordonnées $U$ et $V$ de chaque pixel. On fera de sorte que le pixel au centre de l'image ait pour coordonnées $(0,0)$ : \\
			\texttt{[U V] = meshgrid(-w/2+1/2:w/2-1/2,-h/2+1/2:h/2-1/2)}
		\item A partir de $U$ et $V$, obtenir la matrice $D$, donnant la distance au centre pour chaque pixel de l'image.
		\item A partir de $D$, en déduire le filtre $H$ de Butterworth (sans utiliser de boucle). 
		\item Tracer le filtre en 2D, et éventuellement en 3D grâce à la fonction \texttt{plot3}. Commenter.
	\end{itemize}
	\reponse{\input{question2-2}}
	
	\item Calculer la transformée de l'image en utilisant la fonction \texttt{fft2}. On n'oubliera pas, après avoir effectué la FFT de l'image, de réarranger les données afin que la fréquence nulle se trouve au centre de l'image (\texttt{fftshift}). Afficher le module de la transformée de Fourier.
	\reponse{\input{question2-3}}
	
	\item Filtrer l'image par $H$ dans le domaine fréquentiel. 
	
	\item Revenir à l'espace direct par transformée de Fourier inverse et afficher l'image filtrée. Commenter.
	\reponse{\input{question2-5}}
\end{enumerate}



\section{Filtrage non linéaire : filtre médian}

L'avantage de filtres définis dans l'espace de l'image par rapport à l'espace de Fourier, est la possibilité de définir des filtres non linéaires. Un exemple typique de filtre non linéaire est le filtre médian. Il permet entre autre, de corriger efficacement un bruit poivre et sel.

\begin{enumerate}[noitemsep, label=\arabic* --]
	\item Rappeler ce qu'est le filtre médian, et plus généralement, ce qu'est un filtre d'ordre.
	\reponse{\input{question3-1}}
	
	\item Rappeler ce qu'est un bruit poivre et sel et justifier l'usage d'un filtre médian pour corriger une image affectée d'un tel bruit.
	\reponse{\input{question3-2}}
	
	\item Lire et afficher l'image 'flower.png'. 
	
	\item Ajouter un bruit poivre et sel de densité 0.5 à l'image en utilisant la commande Matlab \texttt{imnoise}.
	
	\item A l'aide d'un filtre médian, corriger le bruit. Pour cela, on utilisera la commande plus générale liée aux filtres d'ordre \texttt{ordfilt2}. 
	
	\item Afficher l'image filtrée pour plusieurs tailles de filtre et discuter des résultats.
	\reponse{\input{question3-6}}
\end{enumerate}

\vspace{10pt}
\textbf{Fin du rendu obligatoire pour le TP1}
\hrule

\section{Filtrage à partir d'histogramme : seuillage par $K$-means}

\begin{enumerate}[noitemsep, label=\arabic* --]
	\item Lire et afficher l'image 'flower.png'. 
	\item Ecrire une fonction implémentant l'algorithme de $K$-means à $K$ régions :
	\begin{itemize}[noitemsep]
		\item Choisir aléatoirement $K$ intensités $m_i$.
		\item Assigner à chaque pixel le label 1 si son intensité est plus proche de la valeur $m_1$, le label 2 si son intensité est plus proche de la valeur $m_2$, ..., le label $K$ si son intensité est plus proche de la valeur $m_K$ . On stockera ces labels dans la matrice \texttt{labels}.
		\item Mettre à jour les valeurs de $m_i$ en prenant la moyenne des intensités des pixels de label $i$.
		\item Itérer jusqu'à ce que les moyennes $m_i$ ne changent plus.
	\end{itemize}
	\item Afficher l'image segmentée en 2 régions avec l'algorithme des $K$-means. Comment pourrait-on remonter à un seuil à partir de cet algorithme ?
	\item Discuter du choix de $K$ pour la segmentation de l'image 'flower.png'.
\end{enumerate}

\end{document}
