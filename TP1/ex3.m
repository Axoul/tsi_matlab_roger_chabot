close all;
clear variables;

I = imread('flower.png');
I2n = imnoise(I,'salt & pepper',0.5);
I2 = im2double(I2n);
[h,w] = size(I);

for i = [3 5 7 9]
     domaine = ones(i);
     ordre=(i^2+1)/2-1;
     filtre_median = ordfilt2(I2, ordre, domaine);
     figure;
     imshow(filtre_median);
     xlabel(["Image filtré avec Filtre Median",num2str(ordre)]);
end
