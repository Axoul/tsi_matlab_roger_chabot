close all;
clear variables;

I = imread('pieces.png');
[h,w] = size(I);
N=h*w;
H=imhist(I);
Hn=H/N;
Hc = cumsum(Hn);
Ie = zeros(h,w);
for i=1:h
     for j=1:w
        Ie(i,j) = Hc(I(i,j)+1);
     end
end
m1s =rand()*0.3+0.7;
m2s= rand()*0.3+0.7;
m = max(m1s,m2s);
m2s = min(m1s,m2s);
m1s=m;
m1=-1;
m2=-1;
while m1 ~= m1s && m2 ~= m2s
     m1 = m1s;
     m2 = m2s;
     labels = zeros(h,w);
     labels(abs(Ie-m1) > abs(Ie-m2) )=2;
     labels(abs(Ie-m1) < abs(Ie-m2) )=1;
     m1s = mean2(Ie(labels==1));
     m2s = mean2(Ie(labels==2));
end

Ie(labels==1) = 1;
Ie(labels==2) = 0;

se = strel('disk',3);

dilatedI = imdilate(Ie,se); %Dilatation
IF = imerode(dilatedI,se); %Erosion
figure;
imshow(IF,[]);
title('Fermeture morphologique de limage segmentée en 2 régions');

Iclear = imclearborder(IF);
figure;
imshow(Iclear,[]);
title('Fermeture morphologique de limage nettoyée segmentée en 2 régions')

eulb= 7;
P = zeros(30,1);
figure;
subplot(3,2,1);
imshow(Iclear);
i=2;
for k=1:30
     se = strel('disk',k); %marqueur disk pas optimal
     E = imopen(Iclear,se);
     eul = bweuler(E); %nombre de piece

     if eul < eulb
         P(k) = eulb-eul;
         eulb = eul;

         subplot(3,2,i);
         imshow(E);
         i=i+1;
     end
end
figure(2);
bar(P,0.3,'r');
title('Courbe granulométrique');
xlabel('rayon du disque');
ylabel('nombre de cercle');