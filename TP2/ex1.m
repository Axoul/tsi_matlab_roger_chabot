close all;
clear variables;

I = imread('pieces.png');
I2 = im2double(I);
[h,w] = size(I);
m1s =rand();
m2s= rand();
m1=-1;
m2=-1;
while m1 ~= m1s && m2 ~= m2s
     m1 = m1s;
     m2 = m2s;
     labels = zeros(256);
     labels(abs(I2-m1) > abs(I2-m2) )=2;
     labels(abs(I2-m1) < abs(I2-m2) )=1;
     m1s = mean2(I2(labels==1));
     m2s = mean2(I2(labels==2));
end
I2(labels==1) = m1;
I2(labels==2) = m2;
figure;
imshow(I2,[]);
title('algorithme des K-means pour une segmentation en 2 regions');

ID = im2double(I);
N = h*w;

figure;
subplot(1,3,1);
bar(imhist(ID));
axis([0 256 -inf inf]);
title('Histogramme de limage');
subplot(1,3,2);
bar(imhist(ID)/N);
axis([0 256 -inf inf])
title('Histogramme normalisé de limage');
subplot(1,3,3);
plot(cumsum(imhist(ID)));
axis([0 256 -inf inf])
title('Histogramme cumulé de limage');

H = imhist(ID);
Hn = H/N;
Hc = cumsum(Hn);
Ie = zeros(h,w);
for i=1:h
    for j=1:w
        Ie(i,j) = Hc(I(i,j)+1);
    end
end
k = 1:256;
He = imhist(Ie);
Hen = He/N;
Hec = cumsum(Hen);

figure;
subplot(2,1,1);
imshow(Ie,[]);
title('Image égalisée');
subplot(2,1,2);
hold on;
bar(k,Hen);
plot(k,Hec);
hold off;
legend("Histogramme d'églisation normailisé","Histogramme d'églisation cumulé");
axis([0 256 -inf inf]);
% figure;
% imshow(Ie,[]);


m1s =rand()*0.3+0.7;
m2s= rand()*0.3+0.7;
m = max(m1s,m2s);
m2s = min(m1s,m2s);
m1s=m;
m1=-1;
m2=-1;
while m1 ~= m1s && m2 ~= m2s
     m1 = m1s;
     m2 = m2s;
     labels = zeros(h,w);
     labels(abs(Ie-m1) > abs(Ie-m2) )=2;
     labels(abs(Ie-m1) < abs(Ie-m2) )=1;
     m1s = mean2(Ie(labels==1));
     m2s = mean2(Ie(labels==2));
end
Ie(labels==1) = m1;
Ie(labels==2) = m2;

figure;
imshow(Ie,[]);
title('Image égalisée et segmentée en 2 régions');